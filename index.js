

// OBJECTS
	// an object is a data type that is used to represent world objects.
	// it is a collection of related data and/or functionalities or method or array or other data types
	// information stored in objects are represented in key:value" pair(called property)

	// Creating objects using object intializer/object literal notation.
	/*
	Syntax:
		let/const objectName = {
			keyA: valueA,
			keyB: valueB,
			....
		}
	*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
	features: ["send message", "make call", "play games"],
	isWorking: true,
	owner: {
		fullName: "Chris",
		yearOfOwnage: 20
	}
}

console.log(cellphone);



		// Accessing/reading properties
			// Dot notation
				/*
				Syntax:
					objectName.propertyName
				*/

console.log(cellphone.name);
console.log(cellphone.isWorking);
console.log(cellphone.features);
console.log(cellphone.features[1]);

console.log(cellphone.owner);
console.log(cellphone.owner.fullName);

			// Bracket Notation
console.log(cellphone["name"]);
console.log(cellphone["owner"]["fullName"]);




		// Creating  objects usiing Constructor Function
			// 	creates reusable function to create several objects that have the same data structure
			/*
			Syntax:
				function objectName(valueA,valueB){
					this.propertyA =valueA,
					this.propertyB =valueB,
				}
			*/
// Example #1
	//this keyword allows us to assign object properties by associating them with the values received from a constructor function's parameter

	function Laptop(name,manufactureDate,RAM){
		this.LaptopName = name,
		this.LaptopmanufactureDate = manufactureDate,
		this.LaptopRAM = RAM
	}

	// Instantiation
		// "new" keyword creates an instance of an object
	let laptop = new Laptop ("Lenovo", 2008, "2gb RAM")
	console.log(laptop)

	let myLaptop = new Laptop ("Macbook Air", "8gb",false)
	console.log(myLaptop);

	let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500mb", false);
	console.log(oldLaptop)



// Example #2
	function Menu(mealName, mealPrice) {
		this.menuName = mealName
		this.menuPrice = mealPrice
	}

	let mealOne = new Menu("Breakfast", 299);
	console.log(mealOne);

	let mealTwo = new Menu("Dinner", 500);
	console.log(mealTwo)



	// Create empty objects

	let computer = {};
	let emptyObject = new Object()
	console.log(computer)
	console.log(emptyObject)



	// Access objects inside an array
	let array = [laptop, myLaptop];
	console.log(array)

	// 

	console.log(array[0].laptopName)
	console.log(array[1].laptopmanufactureDate)



// [SECTION] Initialization/Adding/Deleting/Reassigning Object Properties
	// Like any other variable in JS, objects have their properties initialized or added after the object was created/declared

	let car = {};
	console.log(car)

		// Initialize/Add object property using .notation
		/*
		Syntax:
			objectName.propertyToBeAdded = value;
		*/

	car.name = "Honda Civic";
	console.log(car)


		// Initialize/Add object property using bracket notation
		/*
		Syntax:
			objectName["propertyToBeAdded"] = value;
		*/
	car["manufactureDate"] = 2019;
	console.log(car);




		// Delete object property using dot notation
		/*
		Syntax:
			delete objectName.propertyToBeDeleted;
		*/

	delete car.name;
	console.log(car);


		// Delete object property using bracket notation
		/*
		Syntax:
			delete objectName["propertyToBeDeleted"]
		*/
	delete car["manufactureDate"];
	console.log(car);




		// Reassigning object properties using dot notation

	car.name = "Honda Civic";
	car["manufactureDate"] = 2019;
	console.log(car)

			car.name = "Dodge Charger R/T";
			console.log(car)

		// Reassigning object properties using bracket notation
			car["name"] = "Jeepney";
			console.log(car);


//REMINDER: If you will add property to the object, make sure the property name you will add is not yet existing or ELSE it will just reassign the value of the property.


// [SECTION] Object Methods
	// A method is a function which is a property of an object
	// they are also functions and one of the key differences
	// useful in creating reusable function

			let person = {
				name: "John Doe",
				talk: function(){
					console.log("Hello my name is " + this.name)
				}
			}

			console.log(person);
			person.talk() //use to target or invoke


		person.walk = function() {
			console.log(person.name + " walked 25 steps forward")
		}

		console.log(person);
		person.walk();

	// constructor function with method
		function Pokemon(name, level){
		//Properties of the object
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		// Method
		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
			
			let hpAftertackle = targetPokemon.pokemonHealth- this.pokemonAttack;
			
			console.log(targetPokemon.pokemonName + " health is now reduced tp " + hpAftertackle);
		}

		this.faint = function(){
			console.log(this.pokemonName + " fainted!")
		}

	}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu)
	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados)

	pikachu.tackle(gyarados);

	gyarados.tackle(pikachu);

	pikachu.faint();

